# Chip8Emu
CHIP8 emulator written in Lua, powered by [LuaJIT](http://luajit.org/)'s FFI 
and makes use [LÖVE](https://love2d.org/) for input handling, graphics and 
stuff.

## History
CHIP-8 is an old interpreted language, used on some devices like the Cosmac VIP
and HP48 calculator; it was focused for making games and quite a few were made 
and were popular. Some games like those are BLINKY, SPACE INVADERS and the 
mythical PONG. 

The language itself defined a VM on which it would run programs on, that VM 
featured some registers, a stack, up to 4KiB of memory and a 64x32 monochrome 
screen. For further details on CHIP8's specification read on 
[Matthew Mikolay's paper](http://mattmik.com/files/chip8/mastering/chip8.html) 
and [Cowgod's CHIP8 Technical Reference](devernay.free.fr/hacks/chip8/C8TECH10.HTM)

## Usage
For now to load a game (ROM) into the emulator you have to pass it as an 
argument by the CLI, in the future I'll add some GUI for ROM loading and 
other tweaks.<br>

\*\*\*Since many ROMs are copyrighted I'll just put a link to a site that holds many ROMs for download: [Zophar's Domain](https://www.zophar.net/pdroms/chip8/chip-8-games-pack.html).\*\*\*

`love . <path to ROM>`<br>

The keys at use are:<br>
`1 2 3 4`<br>
`Q W E R`<br>
`A S D F`<br>
`Z X C V`<br>

And there are extra keys for handling emulator cycles per frame (`[` and `]`), debug
mode (`o`), restart the emulation (`p`), pause emulation (`space`), toggle 
compatibility mode for some old games (`i`), executing one instruction at a time
(`n`), loading (`F1..F12`) and saving (`Shift + F1..F12`) emulation states.<br>


## Features
###### - Full support of CHIP8 specification (there may be some quirks but that specification is old and ambiguous).
###### - ROM loading. (duh!)
###### - Load and Save states.
###### - Cycles per frame throttling.
###### - Minimal debug mode (Improvement Coming Soon™)


## License
This project was licensed with GNU GPLv2 and was changed to the current license in use: MIT License.
For details, see [LICENSE](https://gitlab.com/No-one-you-know/Chip8Emu/blob/master/LICENSE).
